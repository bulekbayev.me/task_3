import java.util.Scanner;

public class Solution {

    public static void main(String[] args) throws Exception {
        System.out.println("Enter year:");
        int year = enterYearAndMonth();
        System.out.println("Enter month:");
        int month = enterYearAndMonth();
        checkLeapYear(year, month);
    }

    //Ввод значений с консоли и проверка на отрицательные значения
    public static int enterYearAndMonth() throws Exception {
        Scanner sc = new Scanner(System.in);
        int temp = sc.nextInt();
        if (temp <= 0) {
            throw new Exception("Entered integers must be bigger than 0!");
        } else {
            return temp;
        }
    }

    //Проверка на високосный год
    public static void checkLeapYear(int year, int monthCode) throws Exception {
        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
            System.out.println("```" + "\n" +
                               year + " " + getMonthName(monthCode) + " - leap year" + "\n" +
                               "```");
        } else {
            System.out.println("```" + "\n" +
                               year + " " + getMonthName(monthCode) + " - common year" + "\n" +
                               "```");
        }
    }

    //Соответствие кода_месяца и имени_месяца, с проверкой заданного диапазона значений от 1 до 12
    public static String getMonthName(int monthCode) throws Exception {
        String[] monthNameArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        if (monthCode > 12) {
            throw new Exception("Month must be in range of 1 to 12");
        } else {
            return monthNameArray[monthCode - 1];
        }
    }
}